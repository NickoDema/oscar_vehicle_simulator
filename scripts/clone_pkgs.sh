#!/bin/bash

#############################################################################
# Copyright 2023 ScPA StarLine Ltd. All Rights Reserved.                    #
#                                                                           #
# Created by Ivan Shevtsov "ishevtsov0108@gmail.com"                        #
#                                                                           #
# Licensed under the Apache License, Version 2.0 (the "License");           #
# you may not use this file except in compliance with the License.          #
# You may obtain a copy of the License at                                   #
#                                                                           #
# http://www.apache.org/licenses/LICENSE-2.0                                #
#                                                                           #
# Unless required by applicable law or agreed to in writing, software       #
# distributed under the License is distributed on an "AS IS" BASIS,         #
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  #
# See the License for the specific language governing permissions and       #
# limitations under the License.                                            #
#############################################################################

RETURN_PATH=$PWD

cd "$( dirname "${BASH_SOURCE[0]}" )/../ws/src"

git clone https://gitlab.com/starline/alpha/alpha_gazebo/alpha_emu.git
git clone https://gitlab.com/starline/alpha/alpha_gazebo/alpha_emu_gazebo_plugin.git
git clone https://gitlab.com/starline/alpha/alpha_gazebo/alpha_emu_msgs.git
git clone https://gitlab.com/starline/alpha/alpha_gazebo/alpha_gazebo_vehicles.git
git clone https://gitlab.com/starline/alpha/alpha_gazebo/alpha_gazebo_worlds.git

cd ${RETURN_PATH}