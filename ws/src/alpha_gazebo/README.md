# alpha_gazebo

ROS2 пакет с файлами для запуска проекта в различных сценариях в симуляторе gazebo.

## Работа с пакетом

Выполните [инструкции для сборки симуляции](https://gitlab.com/sl_prak/alpha_gazebo/-/blob/develop/README.md) и перейдите в папку docker/ и запустите docker контейнер:

```shell
bash <path-to-this-rep>/docker/run.bash
```

## Запуск нескольких моделей транспортного средства в Gazebo

Для запуска произвольного количества моделей запустите следующую команду:

```shell
ros2 launch oscar_gazebo multistart.launch.py models_count:="5"
```

Также доступны следующие параметры файла запуска:

1. use_sim_time

* описание: true, тогда с запуском gazebo начнётся симуляция (движение объектов, таймер).
* значение по умолчанию: 'true'

2. model_ns

* описание: базовое название пространства имён ROS модели, к нему добавится уникальное число.

* значение по умолчанию: 'actros_ns'

3. model_name

* описание: имя модели в gazebo.

* значение по умолчанию: 'example_name'

4. world_file_name

* описание: имя файла с миром в alpha_gazebo_worlds/worlds, в котом будут запускатья модели. [Подробнее о добавлении своих миров.](https://gitlab.com/sl_prak/alpha_gazebo_worlds/-/blob/develop/README.md)

* значение по умолчанию: 'car.world',  

5. xacro_dir_name

* описание: имя запускамой модели из alpha_gazebo_vehicle/urdf, [подробнее о добавлении своих моделей.](https://gitlab.com/sl_prak/alpha_gazebo_vehicles/-/blob/develop/README.md)

* значение по умолчанию: 'actros'

6. models_count

* описание: количество моделей, которое нужно запустить в мире.

* значение по умолчанию: '1'


## Запуск модели под управлением ПАК Alpha

Для запуска транспортного средсва, управляемого по средствам [ПАК Alpha](https://gitlab.com/starline/alpha) выполните:

```shell
ros2 launch alpha_gazebo simulation_with_alpha.launch.py
```

Для запуска можно опционально использовать следующие параметры:

***verbose***, ***use_sim_time***, ***model_ns***, ***model_name***, ***world_file_name***, ***xacro_dir_name***

Описание и значение по умолчанию аналогично предыдущему пункту. [Подробнее о запуске AlphaEmu](https://gitlab.com/sl_prak/alpha_emu/-/blob/develop/readme.md).
