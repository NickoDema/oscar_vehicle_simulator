#!/usr/bin/env bash

#############################################################################
# Copyright 2023 ScPA StarLine Ltd. All Rights Reserved.                    #
#                                                                           #
# Created by  Nikolay Dema <ndema2301@gmail.com>                            #
#         and Ivan Shevtsov "ishevtsov0108@gmail.com"                       #
#                                                                           #
# Licensed under the Apache License, Version 2.0 (the "License");           #
# you may not use this file except in compliance with the License.          #
# You may obtain a copy of the License at                                   #
#                                                                           #
# http://www.apache.org/licenses/LICENSE-2.0                                #
#                                                                           #
# Unless required by applicable law or agreed to in writing, software       #
# distributed under the License is distributed on an "AS IS" BASIS,         #
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  #
# See the License for the specific language governing permissions and       #
# limitations under the License.                                            #
#############################################################################

NO_COLOR='\033[0m'
GREEN='\033[0;32m'
RED='\033[0;31m'
WHITE='\033[34m'
YELLOW='\033[33m'
BOLD='\033[1m'

function info() {
    (>&2 echo -e "[${WHITE}${BOLD}INFO${NO_COLOR}] $*")
}

function error() {
    (>&2 echo -e "[${RED}ERROR${NO_COLOR}] $*")
}

function warning() {
    (>&2 echo -e "${YELLOW}[WARNING] $*${NO_COLOR}")
}

function ok() {
    (>&2 echo -e "[${GREEN}${BOLD} OK ${NO_COLOR}] $*")
}

