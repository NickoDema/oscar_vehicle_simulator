#!/usr/bin/env bash

#############################################################################
# Copyright 2022 ScPA StarLine Ltd. All Rights Reserved.                    #
#                                                                           #
# Created by  Nikolay Dema <ndema2301@gmail.com>                            #
#         and Ivan Shevtsov "ishevtsov0108@gmail.com"                       #
#                                                                           #
# Licensed under the Apache License, Version 2.0 (the "License");           #
# you may not use this file except in compliance with the License.          #
# You may obtain a copy of the License at                                   #
#                                                                           #
# http://www.apache.org/licenses/LICENSE-2.0                                #
#                                                                           #
# Unless required by applicable law or agreed to in writing, software       #
# distributed under the License is distributed on an "AS IS" BASIS,         #
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  #
# See the License for the specific language governing permissions and       #
# limitations under the License.                                            #
#############################################################################


REP_ROOT="$( cd "$( dirname "${BASH_SOURCE[0]}" )/.." && pwd )"

source "${REP_ROOT}/docker/show_output.bash"


### Check if NVIDIA GPU flag is needed ---------------------------------- #

if [[ $1 != "--nongpu" ]] && [ -n "$(which nvidia-smi)" ] && [ -n "$(nvidia-smi)" ]; then

    SOURCE_IMG_NAME="nvidia/opengl:1.1-glvnd-runtime-ubuntu22.04"
    TAG="ub22.04_nvidia"

else
    SOURCE_IMG_NAME="ubuntu:22.04"
    TAG="ub22.04"
fi


### Docker build -------------------------------------------------------- #

docker build -t alpha_gazebo:${TAG}                                 \
             -f ${REP_ROOT}/docker/Dockerfile ${REP_ROOT}           \
                             --network=host                         \
                             --build-arg base_img=$SOURCE_IMG_NAME


### Show result --------------------------------------------------------- #

if [ $? -ne 0 ]; then
    error "Docker image building error. Smth wrong!"
    exit 1
fi

ok "Done building. Enjoy!"
